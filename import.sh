#!/bin/bash/
# This script must be run with bash (dash for instance will not accept "<<<")

OS=$(grep "^ID_LIKE=" /etc/os-release)
OS=$(sed -e 's#.*=\(\)#\1#' <<<$OS)
OS=$(sed 's/"//g' <<<$OS)

printf "The  current OS is $OS\n"

# Check if the OS corresponds to a known one, if not exit with error
[[ $OS != "manjaro" && $OS != "arch" && $OS != "ubuntu" && $OS != "debian" && $OS != "rhel fedora" ]] && ( printf "OS not supported\n" ; exit 1 )

printf "\n########## Install prerequisites ##########\n"
case $OS in
manjaro|arch)
    sudo pacman -S --noconfirm --needed wget python-pynvim
    ;;
ubuntu|debian)
    sudo apt -qq install -y wget
    ;;
"rhel fedora")
    sudo dnf install -y wget -q
    ;;
esac

printf "\n########## Remove previous installs ##########\n"
case $OS in
manjaro|arch)
    sudo pacman -R --noconfirm neovim vim
    ;;
ubuntu|debian)
    sudo apt -qq remove -y neovim vim
    ;;
"rhel fedora")
    sudo dnf remove -y neovim vim -q
    ;;
esac

rm -rf ~/.config/nvim*

printf "\n########## Install neovim and dependencies ##########\n"
case $OS in
manjaro|arch)
    sudo pacman -Syu --noconfirm
    sudo pacman -S --noconfirm neovim python-pynvim
    ;;
ubuntu|debian)
    sudo apt -qq update
    sudo apt -qq install -y neovim
    ;;
"rhel fedora")
    sudo dnf update -y -q
    sudo dnf install -y neovim -q
    ;;
esac

printf "\n########## Get neovim config ##########\n"
mkdir ~/.config/nvim
mkdir ~/.config/nvim/vim-plug
mkdir ~/.config/nvim/autoload
wget https://gitlab.com/myscripts12/myneovim/-/raw/master/nvim/init.vim -O ~/.config/nvim/init.vim
wget https://gitlab.com/myscripts12/myneovim/-/raw/master/nvim/vim-plug/plugins.vim -O ~/.config/nvim/vim-plug/plugins.vim
