" Load vim plugins
	source $HOME/.config/nvim/vim-plug/plugins.vim

" Fundamentals settings
	syntax on
	
" Looking settings
	set number
	set relativenumber
	set cursorline
	colorscheme onedark

" Mapping leader key to "Space"
	map <SPACE> <Leader>

" Mappings	
	nnoremap <leader>t <cmd>Deol<cr>

" Set "-" to choose windows with "choosewin"
	nmap  -  <Plug>(choosewin)
" if you want to use overlay feature
	let g:choosewin_overlay_enable = 1

" let use of which key pattern
  let g:which_key_map = {}
  autocmd! User vim-which-key call which_key#register('<Space>', 'g:which_key_map')

" vim-which-key
	nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
  " By default timeoutlen is 1000 ms
  set timeoutlen=1500
  let g:mapleader = "\<Space>"
	let g:maplocalleader = ','
	nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
	nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>
	let g:which_key_map.w = {
      \ 'name' : '+windows' ,
      \ 'w' : ['<C-W>w'     , 'other-window']          ,
      \ 'd' : ['<C-W>c'     , 'delete-window']         ,
      \ '-' : ['<C-W>s'     , 'split-window-below']    ,
      \ '|' : ['<C-W>v'     , 'split-window-right']    ,
      \ '2' : ['<C-W>v'     , 'layout-double-columns'] ,
      \ 'h' : ['<C-W>h'     , 'window-left']           ,
      \ 'j' : ['<C-W>j'     , 'window-below']          ,
      \ 'l' : ['<C-W>l'     , 'window-right']          ,
      \ 'k' : ['<C-W>k'     , 'window-up']             ,
      \ 'H' : ['<C-W>5<'    , 'expand-window-left']    ,
      \ 'J' : [':resize +5'  , 'expand-window-below']   ,
      \ 'L' : ['<C-W>5>'    , 'expand-window-right']   ,
      \ 'K' : [':resize -5'  , 'expand-window-up']      ,
      \ '=' : ['<C-W>='     , 'balance-window']        ,
      \ 's' : ['<C-W>s'     , 'split-window-below']    ,
      \ 'v' : ['<C-W>v'     , 'split-window-below']    ,
      \ '?' : ['Windows'    , 'fzf-window']            ,
      \ }

  let g:which_key_map['e'] = [ ':CocCommand explorer'   , 'open explorer' ]
  let g:which_key_map['t'] = [ ':Deol'                  , 'open terminal' ]

  " coc-prettier
  command! -nargs=0 Prettier :CocCommand prettier.formatFile

  " vim-autoformatter
  let g:python3_host_prog='/usr/bin/python3'
