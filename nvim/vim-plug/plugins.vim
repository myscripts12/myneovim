" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " Coc Plugin 
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " CoC file explorer
    Plug 'weirongxu/coc-explorer'
    " Coc Plugin for json
    Plug 'neoclide/coc-json'
    " Navigate thru windows
    Plug 't9md/vim-choosewin'
    " Leader key helper
    Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Onedark theme
    Plug 'joshdick/onedark.vim'    
    " Deol.vim 
    Plug 'shougo/deol.nvim'
    " Startup page
    Plug 'mhinz/vim-startify'
    " Font
    Plug 'ryanoasis/vim-devicons'
    " Autoformatter
    Plug 'chiel92/vim-autoformat' 
    " Status/tabline
    Plug 'vim-airline/vim-airline'
call plug#end()
