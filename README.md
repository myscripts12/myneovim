# [DEPRECATED] see https://gitlab.com/cybernemo/dotfiles
# MyVim

This repository contains my NeoVIM configuration. There is an import, export and update script. The import script will import the current configuration. The export script will export the configuration to this repo. The update script will only update the configuration without reinstalling everything.

## Supported OS

Currently the following OSes are supported:

- [x] Centos 8
- [x] Manjaro
- [x] Ubuntu 20.04
- [x] Debian
- [x] ArchLinux

## Export

The export script will export the current neoVIM configuration to this repo.

```zsh
curl https://gitlab.com/myscripts12/myneovim/-/raw/master/import.sh | bash
```

## Import

The import script will install and import the NeoVIM configuration.

To run it:

```zsh
curl https://gitlab.com/myscripts12/myneovim/-/raw/master/import.sh | bash
```

Once install, start neoVIM, click _Escape_, type `:PlugInstall` and hit enter.

## Update

## TODO
