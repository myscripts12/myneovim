#!/bin/bash/
# Export script of the current neovim configuration
GIT_REPO=$(pwd)

printf "\n########## Remove old config files from repo folder ########\n"
rm -rf $GIT_REPO/nvim

printf "\n########## Copy new config files ##########\n"
cp -R ~/.config/nvim/ $GIT_REPO/nvim/
rm -rf $GIT_REPO/nvim/autoload/

printf "\n########## Git push ##########"
git add .
git commit -m "Update nvim configuration"
git push origin master
